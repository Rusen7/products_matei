#include "NonperishableProduct.h"

NonperishableProduct::NonperishableProduct(uint32_t id, std::string name, float rawPrice, NonperishableProductType type):Product(id, name, rawPrice), m_type(type) {}
NonperishableProductType NonperishableProduct::GetType()
{
	return m_type;
}
uint32_t NonperishableProduct::GetVAT()
{
	return 19;
}
float NonperishableProduct::GetPrice()
{
	return m_rawPrice;
}
