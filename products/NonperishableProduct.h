#pragma once
#include "Product.h"
#include <cstdint>

enum class NonperishableProductType
{
	Clothing,
	SmallAppliences,
	PersonalHygiene
};

class NonperishableProduct: public Product
{
private:
	NonperishableProductType m_type;
public:
	NonperishableProduct(uint32_t id, std::string name, float rawPrice, NonperishableProductType type);
	NonperishableProductType GetType();
	uint32_t GetVAT();
	float GetPrice();
};

