#include "PerishableProduct.h"

PerishableProduct::PerishableProduct(uint32_t id, std::string name, float rawPrice, std::string expirationDate):Product(id, name, rawPrice),m_expirationDate(expirationDate)
{
}

std::string PerishableProduct::GetExpirationDate()
{
	return m_expirationDate;
}

uint32_t PerishableProduct::GetVAT()
{
	return 9;
}

float PerishableProduct::GetPrice()
{
	return m_rawPrice;
}
