#pragma once
#include "Product.h"
#include <cstring>
#include <cstdint>

class PerishableProduct : public Product
{
private:
	std::string m_expirationDate;
public:
	PerishableProduct(uint32_t id, std::string name, float rawPrice, std::string expirationDate);
	std::string GetExpirationDate();
	uint32_t GetVAT();
	float GetPrice();
};

