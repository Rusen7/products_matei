#pragma once
#include <cstdint>
#include <string>
#include "IPriceable.h"

class Product : public IPriceable
{
public:
	Product(uint32_t id, const std::string& name, float rawPrice);
	uint32_t GetId()
	{
		return m_id;
	}
	std::string GetName()
	{
		return m_name;
	}
	float GetRawPrice()
	{
		return m_rawPrice;
	}
protected:
	uint32_t m_id;
	std::string m_name;
	float m_rawPrice;
};

