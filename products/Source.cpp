#include <fstream>
#include <vector>
#include "PerishableProduct.h"
#include "NonperishableProduct.h"
#include <iostream>
#include <iomanip>
#include <algorithm>

void printAll(std::vector<Product*> products)
{
	for (auto& product : products)
	{
		std::cout << product->GetId() << ' ' << product->GetName() << " $" << product->GetRawPrice() << '\n';
	}
}

int main()
{
	std::ifstream productsFile("Products.proddb");
	std::vector<Product*> products;

	uint32_t id;
	std::string name;
	float rawPrice;
	uint16_t vat;
	std::string dateOrType;

	while (!productsFile.eof())
	{
		productsFile >> id >> name >> rawPrice >> vat >> dateOrType;
		if (vat == 9)
		{
			PerishableProduct* product = new PerishableProduct(id, name, rawPrice, dateOrType);
			products.push_back(product);
		}
		else if (vat == 19)
		{
			NonperishableProductType type;
			if (dateOrType == "Clothing")
				type = NonperishableProductType::Clothing;
			else if (dateOrType == "PersonalHygiene")
				type = NonperishableProductType::PersonalHygiene;
			else if (dateOrType == "SmallAppliences")
				type = NonperishableProductType::SmallAppliences;
			NonperishableProduct* product = new NonperishableProduct(id, name, rawPrice, type);
			products.push_back(product);
		}
	}

	std::cout << "Non-perishable products: \n";
	for (auto& product : products)
	{
		NonperishableProduct* castProduct = dynamic_cast<NonperishableProduct*>(product);
		if (castProduct != nullptr) // successful cast at runtime, i.e is nonperishable
		{
			std::cout << castProduct->GetName() << "\n";
			float price = castProduct->GetPrice();
			uint32_t vat = castProduct->GetVAT();
			float priceExclVat = (100.0 * price) / (100.0 + vat);
			std::cout << std::fixed << std::setprecision(2) << "Price excluding VAT: $" << priceExclVat << '\n';
		}
	}
	std::cout << "\nAll products, sorted by NAME: \n";
	std::sort(products.begin(), products.end(), [](Product* p1, Product* p2) -> bool {return p1->GetName().compare(p2->GetName()) < 0; });
	printAll(products);
	std::cout << "\nAll products, sorted by PRICE: \n";
	std::sort(products.begin(), products.end(), [](Product* p1, Product* p2) -> bool {return p1->GetPrice() < p2->GetPrice(); });
	printAll(products);
	return 0;
}
